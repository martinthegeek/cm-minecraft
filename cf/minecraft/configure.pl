# cf instructions for minecraft server

use strict;

# Given a list of strings, returns text body with one string per line (and sorted)
sub SortListToText {
    my @items = sort(@_);
    return join("\n", @items) . "\n";
}


# Ensure we are configured as a minecraft server
sub configure {

    # Configure debconf to access oracle license (for java)
    $svc_package->install("debconf-utils");
    if (read_command("debconf-get-selections") !~
        'oracle-java7-installer.*shared/accepted-oracle-license-v1-1.*true') {
        run("/bin/echo -e oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections");
    }

    # Add webupd8team repo to apt sources
    if (system("grep -q webupd8team/java `find /etc/apt/sources.list /etc/apt/sources.list.d/ -type f`") != 0) {
        if ($? == 256) {
            # Grep returned "1" - no match found. Add ppa repository
            run("add-apt-repository -y ppa:webupd8team/java");
            run("apt-get update");
        } else {
            die "grep of /etc/apt/sources failed: $?\n";
        }
    }

    # Install packages
    $svc_package->install("oracle-java7-installer");
    $svc_package->install("screen");

    # Add user
    $svc_account->addgroup("minecraft");
    $svc_account->adduser(-gid=>"minecraft", -home=>$vardir, -shell=>"/bin/sh", $user);

    # Build home/data directory
    directory(-user=>$user, -mode=>"755", $vardir);

    # Download server software
    my $id = "Tekkit_Lite_Server_$version";
    my $src_url = "http://mirror.technicpack.net/Technic/servers/tekkitlite/$id.zip";
    my $tmp = "$vardir/$id.zip";
    if (!fileinfo($tmp)->exist()) {
        geturl($src_url, $tmp);
    }

    # Extract tekkit software
    my $dest = "$vardir/$id";
    if (!fileinfo("$dest/id")->exist() || read_file("$dest/id") ne $id) {
        directory("$dest~");
        run(-chdir=>"$dest~", "jar -xf $tmp");
        writefile("$dest~/id", $id);
        tidy('--rmdir', $dest);
        run("mv $dest~ $dest");
    }

    # Link to parts tekkit that won't change
    makelink("$vardir/$id/TekkitLite.jar", "$vardir/TekkitLite.jar");
    makelink("$vardir/$id/coremods", "$vardir/coremods");
    makelink("$vardir/$id/mods", "$vardir/mods");

    # Make initial copies of config files that will change    
    foreach my $sub ("config", "server.properties") {
        my $src = "$vardir/$id/$sub";
        my $dest = "$vardir/$sub";
        if (!fileinfo($dest)->exist()) {
            copy(-r=>-1, -user=>$user, $src, $dest);
        }
    }

    # Write out managed config files.
    writefile(-user=>$user, -mode=>"644", "$vardir/ops.txt",
        SortListToText(split(" ", $ops)));
    writefile(-user=>$user, -mode=>"644", "$vardir/banned-players.txt",
        SortListToText(split(" ", $ban)));
    writefile(-user=>$user, -mode=>"644", "$vardir/banned-ips.txt",
        SortListToText(split(" ", $ban_ip)));
    writefile(-user=>$user, -mode=>"644", "$vardir/white-list.txt",
        SortListToText(split(" ", $whitelist)));

    # Install init script, with template substitutions
    my %init_vars = (
        homedir => $vardir,
        user => $user,
	heap_size => $memsize,
	heap_start => $memsize,
    );
    my $content = read_file("$this_dir/minecraft.init");
    # Substitute any ${..} variables in content
    eval { $content =~ s/\$\{(\w+)\}/$init_vars{$1}/g; };
    die "failed substitution for minecraft.init: $@" if $@;
    writefile(-mode=>"0755", "/etc/init.d/$name", $content);

    # Make sure its running and starts at boot
    $svc_service->enable(-script=>"/etc/init.d/$name", $name);
}
