
{% set vardir = pillar.get('minecraft:vardir', '/var/lib/minecraft') %}
{% set memsize = pillar.get('minecraft:memsize', '1024M') %}
{% set ops = pillar.get('minecraft:ops', []) %}
{% set whitelist = pillar.get('minecraft:whitelist', []) %}
{% set ban = pillar.get('minecraft:ban', []) %}
{% set ban_ip = pillar.get('minecraft:ban_ip', []) %}

{% set version = pillar.get('minecraft:version', '0.6.1') %}
{% set sourcehash = pillar.get('minecraft:sourcehash', 'sha1=7f3d00e2d3682d9a02780d16db336b24bc46467b') %}

{% set id = "Tekkit_Lite_Server_" ~ version %}
{% set dest = vardir ~ "/" ~ id %}


# Install java (should move its own module)
# deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu precise main
webupd8team:
    pkgrepo:
        - managed
        - name: "deb http://ppa.launchpad.net/webupd8team/java/ubuntu precise main"

accept-oracle-license:
    cmd.run:
        - name: "echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections"
        - unless: "debconf-get-selections | grep 'oracle-java7-installer.*shared/accepted-oracle-license-v1-1.*true'"

oracle-java7-installer:
    pkg:
        - installed
        - skip_verify: true
        - require:
            - cmd: accept-oracle-license
            - pkgrepo: webupd8team

screen:
    pkg:
        - installed

mygroup:
    group:
        - present
        - name: minecraft

myuser:
    user:
        - present
        - name: minecraft
        - gid_from_name: true
        - home: /var/lib/minecraft

/var/lib/minecraft:
    file.directory:
        - mode: 755
        - user: minecraft

{{ dest }}.zip:
    file.managed:
        - source: http://mirror.technicpack.net/Technic/servers/tekkitlite/{{ id }}.zip
        - source_hash: {{ sourcehash }}
        # dont replace existing - to make timings with alternate CM fair
        - replace: false

unzip:
    cmd.run:
        - name: "mkdir {{ dest }}~ && cd {{ dest }}~ && jar -xf {{ dest }}.zip && echo {{ id }} >id && rm -fr {{ dest }} && mv {{ dest }}~ {{ dest }}"
        - unless: "test `cat {{ dest }}/id` = {{ id }}"
        - require:
            - file: {{ dest }}.zip
            - pkg: oracle-java7-installer

{% for file in 'TekkitLite.jar', 'coremods', 'mods' %}
{{ vardir }}/{{ file }}:
    file.symlink:
        - target: {{ dest }}/{{ file }}
        - require_in:
            - service: minecraft
{% endfor %}

# Local file:// source are not supported yet :-( - otherwise would do this with
# file.managed
{{ vardir }}/server.properties:
    cmd.run:
        - name: "cp {{ dest }}/server.properties {{ vardir }}/server.properties && chown minecraft {{ vardir }}/server.properties"
        - unless: "test -f {{ vardir }}/server.properties"
        - require_in:
            - service: minecraft

{{ vardir }}/config:
    cmd.run:
        - name: "cp -r {{ dest }}/config {{ vardir }}/config && chown -R minecraft {{ vardir }}/config"
        - unless: "test -d {{ vardir }}/config"
        - require_in:
            - service: minecraft

{{ vardir }}/ops.txt:
    file.managed:
        - user: minecraft
        - mode: 644
        - contents: |
            {{ (ops|join("\n")) ~ "\n" }}
        - require_in:
            - service: minecraft

{{ vardir }}/white-list.txt:
    file.managed:
        - user: minecraft
        - mode: 644
        - contents: |
            {{ (whitelist|join("\n")) ~ "\n" }}
        - require_in:
            - service: minecraft

{{ vardir }}/banned-players.txt:
    file.managed:
        - user: minecraft
        - mode: 644
        - contents: |
            {{ (ban|join("\n")) ~ "\n" }}
        - require_in:
            - service: minecraft

{{ vardir }}/banned-ips.txt:
    file.managed:
        - user: minecraft
        - mode: 644
        - contents: |
            {{ (ban_ip|join("\n")) ~ "\n" }}
        - require_in:
            - service: minecraft

/etc/init.d/minecraft:
    file.managed:
        - user: root
        - group: root
        - mode: 0744
        - source: salt://minecraft.init
        - template: jinja
        - defaults:
            myuser: minecraft
            vardir: {{ vardir }}
            min_heap: {{ memsize }}
            max_heap: {{ memsize }}
        - require_in:
            - service: minecraft

minecraft:
    service:
        - running
