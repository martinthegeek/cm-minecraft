#
# Cookbook Name:: tekkit
# Recipe:: default
#
# MIT License
#

include_recipe "screen"
include_recipe "java::oracle"

myuser = node[:tekkit][:user]
version = node[:tekkit][:version]
id = "Tekkit_Lite_Server_#{version}"
vardir = "#{node[:tekkit][:dir]}"
dest = "#{vardir}/#{id}"

user myuser do
  home vardir
end

directory vardir do
  owner myuser
  action :create
end

# Download tekkit zip
remote_file "#{dest}.zip" do
  action :create_if_missing
  owner myuser
  mode 0644
  source "http://mirror.technicpack.net/Technic/servers/tekkitlite/#{id}.zip"
  # This is better than "create_if_missing" - but unfair timing versus others
  # checksum "177de5cfa5945ae14b9bf973643993831f97ab47bd4f93e1e36edb9c85bdc374"
end

# Extract tekkit zip
bash "unzip tekkit" do
    code <<-EOF
        set -e
        mkdir #{dest}~
        cd #{dest}~
        jar -xf #{dest}.zip
        echo #{id} >id
        rm -fr #{dest}
        mv #{dest}~ #{dest}
    EOF
    not_if "test `cat #{dest}/id` = #{id}"
    subscribes :run, "remote_file[#{dest}.zip]"
end

# Link tekkit files that don't change
%w{TekkitLite.jar coremods mods}.each do |f|
    link "#{vardir}/#{f}" do
        to "#{dest}/#{f}"
        notifies :restart, "service[minecraft]"
    end
end

# Copy files that do change
execute "#{vardir}/server.properties" do
    command "cp #{dest}/server.properties #{vardir}/server.properties && chown #{myuser} #{vardir}/server.properties"
    not_if "test -f #{vardir}/server.properties"
    subscribes :run, "bash[unzip tekkit]"
end

execute "#{vardir}/config" do
    command "cp -r #{dest}/config #{vardir}/config && chown -R #{myuser} #{vardir}/config"
    not_if "test -d #{vardir}/config"
    subscribes :run, "bash[unzip tekkit]"
end

# Generate managed config files
file "#{vardir}/ops.txt" do
    content node[:tekkit][:ops].join("\n")+"\n"
    owner myuser
end

file "#{vardir}/white-list.txt" do
    content node[:tekkit][:whitelist].join("\n")+"\n"
    owner myuser
end

# Generate sysvinit script from template
template "/etc/init.d/minecraft" do
  source "minecraft"
  mode 0755
end

# Start service now and at boot.
service "minecraft" do 
  supports :status => true, :restart => true
  action [:enable, :start]
end
