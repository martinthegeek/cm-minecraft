maintainer       "Martin Andrews"
maintainer_email "martin@sharpcontrast.com"
license          "All rights reserved"
description      "Installs/Configures Tekkit-Lite"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.0.1"
recipe           "tekkit", "Installs Tekkit-Lite server"

%w{ screen java }.each do |cb|
  depends cb
end

%w{ debian ubuntu }.each do |os|
  supports os
end
