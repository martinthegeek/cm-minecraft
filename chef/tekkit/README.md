Description
===========
Installs Tekkit-Lite Server

Loosely based off minecraft module from James Walker (walkah@walkah.net)

Requirements
============

Platform
--------

* Debian, Ubuntu

Tested on:

* Ubuntu 12.04

Cookbooks
---------

* screen
* java

Attributes
==========

* `node['tekkit']['dir']` - Location of the minecraft files. Default is '/home/minecraft'.
* `node['tekkit']['user']` - User to run the server as. Default is 'minecraft'.
* `node['tekkit']['memsize']` - Heap size
* `node['tekkit']['version']` - Tekkit version to install
* `node['tekkit']['ops'] - List of users with admin rights
* `node['tekkit']['whitelist'] - List of users allowed access

License and Author
==================

Author:: Martin Andrews (martin@sharpcontrast.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
