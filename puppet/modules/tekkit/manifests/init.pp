
# Make sure the java needed for tekkit-lite is installed
class tekkit::java {
  class { 'apt': }
  apt::ppa { "ppa:webupd8team/java": }

  exec { "agree-to-jdk-license":
    command => "/bin/echo -e oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections",
    unless => "debconf-get-selections | grep 'oracle-java7-installer.*shared/accepted-oracle-license-v1-1.*true'",
    path => ["/bin", "/usr/bin"],
  }

  package { "oracle-java7-installer": ensure => latest }
}

class tekkit(
  $user          = 'mcserver',
  $group         = 'mcserver',
  $homedir       = '/opt/minecraft',
  $heap_size     = 2048,
  $heap_start    = 512,
  $version       = "0.6.1",
  $ops           = [],
  $whitelist     = [],
  $ban           = [],
  $ban_ip        = [],
)
{
    $id = "Tekkit_Lite_Server_${version}"
    $src_url = "http://mirror.technicpack.net/Technic/servers/tekkitlite/${id}.zip"
    $tmp = "${homedir}/${id}.zip"
    $dest = "${homedir}/${id}"

    require tekkit::java
    package {'screen':
      before => Service['minecraft']
    }

    group { $group:
        ensure => present,
    }
    user { $user:
        gid        => $group,
        home       => $homedir,
        managehome => true,
    }

    # Get the tekkit-lite distribution and extract it.
    exec { "fetch ${id}":
        path    => ['/bin', '/usr/bin', 'sbin', '/usr/sbin'],
        command => "curl -L -o ${tmp}~ ${src_url} && mv ${tmp}~ ${tmp}",
        unless  => "[ -e ${tmp} ]"
    }
    exec { "extract ${id}":
        path    => ['/bin', '/usr/bin', 'sbin', '/usr/sbin'],
        command => "mkdir ${dest}~ && cd ${dest}~ && jar -xf ${tmp} && echo ${id} >id && rm -fr ${dest} && mv ${dest}~ ${dest}",
        unless  => "[ `cat ${dest}/id` = ${id} ]",
        require  => Exec["fetch ${id}"],
    }

    # Link from homedir to the tekkit components that wont change
    file { "${homedir}/TekkitLite.jar":
        ensure => "link",
        target => "${homedir}/${id}/TekkitLite.jar",
    }
    file { "${homedir}/coremods":
        ensure => "link",
        target => "${homedir}/${id}/coremods",
        before => Service['minecraft']
    }
    file { "${homedir}/mods":
        ensure => "link",
        target => "${homedir}/${id}/mods",
        before => Service['minecraft']
    }

    # Copy (dont link) properties and config tree. Dont replace existing files (we may
    # edit them).  It's not proper CM, but good enough for now.
    file { "${homedir}/server.properties":
        ensure  => present,
        replace => false,
        owner => $user, group => $group,
        source => "${homedir}/${id}/server.properties",
        require  => Exec["extract ${id}"],
        before => Service['minecraft']
    }
    file { "${homedir}/config":
        ensure  => present,
        replace => false,
        owner => $user, group => $group,
        source => "${homedir}/${id}/config",
        recurse => true,
        require  => Exec["extract ${id}"],
        before => Service['minecraft']
    }

    file { '/etc/init.d/minecraft':
        ensure  => present,
        owner   => 'root',
        group   => 'root',
        mode    => '0744',
        content => template('tekkit/minecraft_init.erb'),
        before => Service['minecraft']
    }


    # Generate managed config files
    file {
    "${homedir}/ops.txt":
        ensure => present,
        owner => $user, group => $group,
        content => inline_template('<%= @ops.each.map { |s| "#{s}\n" } %>');
    "${homedir}/banned-players.txt":
        ensure => present,
        owner => $user, group => $group,
        content => inline_template('<%= @ban.each.map { |s| "#{s}\n" } %>');
    "${homedir}/banned-ips.txt":
        ensure => present,
        owner => $user, group => $group,
        content => inline_template('<%= @ban_ip.each.map { |s| "#{s}\n" } %>');
    "${homedir}/white-list.txt":
        ensure => present,
        owner => $user, group => $group,
        content => inline_template('<%= @whitelist.each.map { |s| "#{s}\n" } %>');
    }

    # Make sure the service is running and starts at boot
    service { 'minecraft':
        ensure    => running,
        enable => true,
        subscribe => File["${homedir}/TekkitLite.jar"],
    }
}
