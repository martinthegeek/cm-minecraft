
# Every host in our site is a tekkit server!
node default {
    class { 'tekkit':
	heap_size => 512,
	heap_start => 512,
        ops => ["andre21802"],
        whitelist => ["myfriend"],
    }
}

